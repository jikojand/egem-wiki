---
title: Technical Information
description: 
published: true
date: 2022-03-14T22:03:53.108Z
tags: 
editor: markdown
dateCreated: 2019-12-01T20:22:41.582Z
---

# EtherGem / EGEM

The heart and brains of our network for sapphire to leverage, but thats only the start of the network ties.

Algorithm: PoW Dagger-Hashimoto 
# Blocktime
Target: 11-13 seconds
# Ticker Info
Name: EtherGem
Ticker: EGEM 
# Chain Id
Network Id: 1987
Chain Id: 1987
# Ports
RPC:  8895
P2P: 30666
# External Links
BIP-0044
https://github.com/satoshilabs/slips/blob/master/slip-0044.md 

Official Ann
https://bctann.egem.io
https://bitcointalk.org/index.php?topic=3167940.0

# Eras
Blocks and their respected eras. Current era outlined in red.
![egemera4.png](/egemera4.png)
# Supply
After the final era which is the 7th there will be roughly 50 million EGEM in circulation, and each year following will produce around 650k new coins.
# Developer Pay Breakdown
TODO