---
title: EGEM Monsters
description: 
published: true
date: 2020-03-21T18:48:21.398Z
tags: 
---

![egem-monsters-logo.png](/egem-monsters-logo.png)

# Introduction
Monsters are ERC721 tokens. Each monster has the capability to "wear" a Gem in their chest. Each Gem is another ERC721 token, deployed on a different separate contract. ERC721 makes it so that each token is non-fungible, making each Monster and each Gem unique. Players may hold several tokens at the same time, exchange, auction or trade them. Currently, the game is under development.
![egem-monsters-gems.png](/egem-monsters-gems.png)

# Motivation
Long has the EGEM Community waited for the kind of projects that we see everyday developed and deployed for the Ethereum Network. EGEM has the same capabilities (and perhaps even more). EGEM Monsters aims to be a distributed video game developed for the EGEM community, by the EGEM community.

# Genetics
Upon release of the first iteration of the game, each EGEM Monster will have different genetic traits:
![egem-hero-monsters.png](/egem-hero-monsters.png)

# Body Type

- Apple
- Blue
- Earth
- Fire
- Gold
- Love
- Pink
- Sand
- Sea
- Silver

Eye (up to 8 variations)
Nose (up to 8 variations)
Mouth (up to 8 variations)
Horn (up to 8 variations + none)
Ear (up to 8 variations)
Arm (up to 8 variations)
Leg (up to 8 variations)
Tail (up to 8 variations + none)

- This sums up to +212 million different possible monster's phenotypes (212,336,640)

# Reproductive cycle
Upon release, Monsters will be able to pair with each other and produce offspring. Not much is known about the reproductive cycle of these monsters and how they interact with their progeny.

# Gems
Gems will start to be utilized to make Monster and Gemsmonsters able to fight each other. Each Monster has the capability to "bond" with a GEM, making it part of the monster's chest. Upon release, Gems will only be a collectible decorative feature.

It has been stated that Gems will have much more scarcity than Monsters. There is an estimated ratio of 100 monsters per each GEM, meaning not all Monsters will be able to fight at the same time.

There is currently work being done to allow Gems to be detached and reassigned to Monsters but this will most likely incur in some form of cooldown to prevent abuse.

# Fighting
It is estimated that the fighting capabilities will be tied to the Gems. The non-fungibleness of each Gem (ERC721 smart contract) will be closely related to the fighting statistics. In other words: only monsters that have a GEM will be able to fight and the manner in which they fight will depend upon the GEM they're using.

# Custom Premium Monsters
It has been hinted on the Discord channel that a paid service will become available to individuals who would like their monsters to be customized on demand. Example of this will become available upon release, when it has been stated that each Team Member will have a custom made monster made into an avatar. These monsters will be uniquely designed to the user's specifications. For instance, a user might want to have a monster with rainbow coloured skin and a three pointed tail, sunglasses and a creepy smile.

# Team Members
- The team members working on this project are:

Galimba (Lead Developer)
Rotcivegaf (Smart Contracts and Genetic Design)
Osoese (Mobile and API)
Bitjohn (Front End Developer)
Zeniph (Graphics Design)

