---
title: Bot Commands
description: 
published: true
date: 2021-05-16T16:50:08.321Z
tags: 
editor: markdown
dateCreated: 2019-11-29T18:29:14.793Z
---

# Notice
This is the list of all available commands for interacting with our Quarrynode system.

# User Commands
## Discord Specific
- Discord is our interface.

### Private Message
This list is the avaliable commands used via private message to the discord bot.
![egembot.png](/egembot.png)
```
/help - list of commands.
/register - Use to register with our bot.
/setaddress <address> - set the EGEM address for only node rewards on your account.
/setaddresstip <address> - set the EGEM address for only rains/tips/games (can be same address as /setaddress)
/paylimit <amount>- set the limit at which the system sends a transaction for earned credits. Max 100
/autopay - toggle to have bot send earned credits at paylimit.
/notify - toggle to have bot send a message on a down node.
/an tier number ip - Register a node to your account, strict ip policy.
/rn tier number - Remove a node from your account.
/pickavatar var1-33 - Pick the avatar for your node profile on the EGEM website.
/mybal - Return current balance stored in bots database.
/mycreds - Current credits within the EGEM bot.
/mynodes - Shows the number of nodes per tier and will show more stats in future.
/mydata - All info and status of account will be returned.
/mystats - Used to report node stats back to user (UNDER CONSTRUCTION).
/check ip - Used to check on a node by entering its ip.
/snapshot address - See what your balance was at time of snapshot.
/wdmn amount - withdraw node reward credits
/wd amount - Withdraw tip credit amount to your wallet or just use /wd to withdraw all.
/dp txid - Deposit to play the minigames, or send a tip to a user.
        1) Send to bot address 0xe3696bd9aeec0229bb9c98c9cc7220ce37852887 from your tip address
        2) Get the txid from that transaction to use in the command
/teleid telegramid - Assign your telegram id so you can use the cross platform features.
```
### Public Room
This is the list of commands used via the public bot room.
```
/tip @user amount - send a user a tip as a gift from earned credits.
/stats - N/A
/payments - N/A
/bal address - Lookup a EGEM address balance.
/encode message - N/A
/decode message - N/A
```
### Games Room
This is the list of commands used via the public games room.
```
/spin amount - Spin for symbols game, disable autopay if using.
/roll amount - Roll for a chance at getting some EGEM, kinda like a faucet.
/blackjack amount - N/A NOT ONLINE COMING SOON!.
```


# Admin Only Commands
- For the admins and mods of the EGEM community to manage the bot with.

```
/owners - list owners of the bot multisig.
/motd - sets the message of the day displayed every 3 hours and on website.
/requestfunds value - top up bot by starting the process with this command.
/setaddy @user address - Allows admins to alter a users EGEM address.
/rainonline value - for only ONLINE discord users.
/raineveryone value - for all ONLINE/BUSY/AFK users.
```

# SSH/Server Commands
- If using our node system you have certain commands avaliable for use.

```
N/A
```

