---
title: Discord bot Information
description: 
published: true
date: 2019-12-04T15:29:20.297Z
tags: 
---

# The Discord bot
![egembot.png](/egembot.png)

The Discord bot was written from the ground up by Riddlez666
with node.js and the discord.js library. The bot uses mysql for the
database.

## Games
The bot has games as well in the #👾-bot-games channel.

Dice roll:
![roll.png](/roll.png)

Slot machine:

![slots.png](/slots.png)

---
The bot can also be a little sassy at times.

![sassybot.png](/sassybot.png)

## Commands

Information on the Discord commands can be found at
https://wiki.egem.io/botcmds