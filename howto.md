---
title: Mining Information
description: 
published: true
date: 2019-12-01T20:56:18.803Z
tags: 
---

# ASIC Miners
TODO

# GPU Miners
BMiner
https://www.bminer.me/

Claymore Dual Miner
https://bitcointalk.org/index.php?topic=1433925.0

ETH Miner
https://github.com/ethereum-mining/ethminer/releases

PhoenixMiner
https://bitcointalk.org/index.php?topic=2647654.0

# CPU MINERS
TODO

# OS Miners
ethOS Mining OS
http://ethosdistro.com/

Hive OS
https://hiveos.farm/

SMOS
https://simplemining.net/

# Startup Lines

## - Ethminer
### Linux
./ethminer -P stratums://0x87045b7badac9c2da19f5b0ee2bcea943a786644.RIGNAME@pool.egem.io:PORT 

### NVIDIA
ethminer.exe -S pool.egem.io:8004 -O 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7.rigname -U -SP 1

### AMD/ATI
ethminer.exe -S pool.egem.io:8004 -O 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7.rigname -G -SP 1

## - CLAYMORE

### NVIDIA
EthDcrMiner64.exe -epool stratum+tcp://pool.egem.io:8008 -ewal 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7 -epsw x -allpools 1 -allcoins 1 -gser 2 -eworker rigname

SSL/TLS: (example: -epool ssl://pool.egem.io:PORT) 

## - PHOENIX MINER

### NVIDIA and AMD
PhoenixMiner.exe -pool pool.egem.io:8008 -wal 0xc393659c2918a64cdfb44d463de9c747aa4ce3f7 -worker rigname

SSL/TLS: (example: -pool ssl://pool.egem.io:PORT) 