---
title: EGEM Wallets and Storage
description: 
published: true
date: 2022-01-30T14:05:32.716Z
tags: 
editor: markdown
dateCreated: 2019-12-01T19:43:38.306Z
---

# Web Wallet
https://wallet.egem.io

# EGEM Android
**Current Android beta release:**
[egemOpalMobile0.9.3.1.apk](https://cdn.discordapp.com/attachments/423449110046179328/738441101911916636/egemOpalMobile0.9.3.1.apk)

**Tutorial**
docx: [EGEM_wallet_use.docx](https://gitlab.com/ethergem/egem-mobile/-/raw/master/EGEM_wallet_use.docx)
PDF: [egem_wallet_use.docx.pdf](/egem_wallet_use.docx.pdf)

# OPAL
## Download
https://gitlab.com/ethergem/Opal-Alpha-0-0-1

# Metamask
## Download
https://metamask.io/

# MyCrypto
https://www.mycrypto.com/
They also have a windows app that works with egem

# MEW
https://www.myetherwallet.com/
https://wallet.egem.io/
The EGEM network is in the list just select it:
![mew.png](/mew.png)
## Connect (MEW/Metamask settings)
Go under custom RPC and enter the following.

![egemmetamasksetup.png](/egemmetamasksetup.png)

## Connect (Brave Browser settings)
Enter the following.
![egembrave.png](/egembrave.png)

## Keystore/JSON

Step 1. Head to https://www.myetherwallet.com/

Step 2. Click Ã¢â‚¬ËœCreate a New WalletÃ¢â‚¬â„¢.

Step 3. Choose the Ã¢â‚¬ËœBy JSON FileÃ¢â‚¬â„¢ option.

Step 4. Enter a strong password, and write it down on paper.

> We cannot recover or reset your password for you. Write it down! 
{.is-warning}

Step 5. Download your JSON File, preferably to a USB device.

*Do not open this file. It is meant to be used by our interface.*

Step 6. Now youÃ¢â‚¬â„¢re done, and ready to access your wallet!

## Mnemonic Phrase

Step 1. Head to https://www.myetherwallet.com/ 

Step 2. Click Ã¢â‚¬ËœCreate a New WalletÃ¢â‚¬â„¢.

Step 3. Choose the Ã¢â‚¬ËœBy Mnemonic PhraseÃ¢â‚¬â„¢ option.

Step 4. Enter a strong password, and write it down on paper.

> We cannot recover or reset your password for you. Write it down!
{.is-warning}


Step 5. Choose a 12 or 24 word phrase, and WRITE IT DOWN!

> Do not store this on your computer. It is meant to be written down multiple times.
{.is-warning}


Step 6. Now pass a quick test, and youÃ¢â‚¬â„¢re done! YouÃ¢â‚¬â„¢re ready to access your wallet.

# CLI

# Hardware

## Trezor
[trezor](https://wiki.egem.io/Links#wallets)

## Ledger Nano
[ledger nano](https://wiki.egem.io/Links#wallets)
