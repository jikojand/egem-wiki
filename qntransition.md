---
title: Quarrynode Transition
description: 
published: true
date: 2021-05-15T07:52:52.138Z
tags: 
editor: markdown
dateCreated: 2019-12-01T21:05:28.894Z
---

- Here is the information required to be ready for the bot system.

# Requirements
- 1 Discord account.
- 1 EGEM address.
- Proper Balance

1. All the nodes/ips you own currently accross all of your possible accounts on a list for ease of entering into new system.
2. Proper Balance on the account to trigger the proper pay for the nodes you have added.

# Move funds
You need to move all the funds you have on the other accounts/wallets to one main wallet thats attached to the new system.

# Interaction
The main part of the bot is now Direct Message ONLY, so you will have to send a message to our bot named "The EGEM Master" which you can find in the users list.

![egembot.png](/egembot.png)
# Register
To check if you are not in the system a simple /register to the bot will either add you or return that you are already registered (Old accounts imported earlier).
```
/register
```

# Set Address
You only need to set your address if you are a new user or did not make the cutoff for the database move.
```
/setaddress address
```
# EXAMPLES
```
Example for Existing user Quarrynode transition

User of old bot has 3 tier 1 and 3 tier 2 nodes

Q: Would you like that in one EGEM account?

A: User would like to keep seperate 
 
         For this to be accomplished 3 discord accounts are needed
         on each account run the following

 make sure egem address has 40,000+ EGEM

 /an 1 1 192.168.1.1 (change to own vps ip)
 /an 2 1 192.168.1.2
```

---
```
New Quarrynode user has 2 nodes

Step 1

Assign the EGEM address so the system can watch the balance and send transactions to it.

/setaddress egemaddress

Step 2

make sure egem address has 40,000+ EGEM

 /an 1 1 192.168.1.1 (change to own vps ip)
 /an 2 1 192.168.1.2

```

# Registering nodes
https://wiki.egem.io/qnregister