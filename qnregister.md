---
title: Quarrynode Registration
description: 
published: true
date: 2021-05-15T07:51:50.126Z
tags: 
editor: markdown
dateCreated: 2019-12-01T20:42:25.509Z
---

# Requirements

- Tier 1 Node**: 10k (ten thousand) or more EGEM on a registered wallet
- Tier 2 Node**: a Tier 1 Node + 30k (thirty thousand) or more EGEM on the same registered wallet (a total minimum of 40k or more EGEM on the same wallet)  

## Example
- An **Ubuntu 18.04 VPS** with **IPv4** for each tier
- Tier 1 (10000 EGEM, 1 VPS, 1 IPv4)
### **Geode = 40k (and consists of TWO nodes a 10k tier1 and a 30k tier 2)**
- For each Tier 2 you must Have a Tier 1 (A Complete Geode)
- 1 Geode (40000 EGEM, 2 VPS's, 2 IPv4's)
- 3 Geodes (120000 EGEM, 6 VPS's, 6 IPv4's)

# Register
> The system allows you to use discord to manage your nodes and system features.
{.is-success}

## Discord
https://discord.egem.io

Join the discord server and look for our bot called "The EGEM Master" then send it a private message to start using system.

![egembot.png](/egembot.png)

# Add a node
[Full Bot Command List](https://wiki.egem.io/botcmds)  
From within discord use the following command.
> WARNING!!! It is very important to make sure it is either tier 1 or 2 selected for tier, and the node number is from 1 to 10.
{.is-warning}

```/an tier number ipaddress```
## Examples
Here are some examples of certain scenarios for users and nodes with required balances.
> **It is recommended to make a list of your nodes and IP before you add them to the bot**
{.is-warning}


> **A user with 10,000+ EGEM**
>/an tier number ipaddress
> /an 1 1 192.168.1.1
{.is-success}


> **A user with 40,000+ EGEM**
>/an tier number ipaddress
> /an 1 1 192.168.1.1
> /an 2 1 192.168.1.2
{.is-success}


> Below examples are seperated by geode (tier1 and tier 2) 
{.is-info}

> **A user with 200,000+ EGEM**
> /an tier number ipaddress
> /an 1 1 192.168.1.1
> /an 2 1 192.168.1.2
{.is-success}


> /an 1 2 192.168.1.3
> /an 2 2 192.168.1.4
{.is-success}

> /an 1 3 192.168.1.5
> /an 2 3 192.168.1.6
{.is-success}

> /an 1 4 192.168.1.7
> /an 2 4 192.168.1.8
{.is-success}

> /an 1 5 192.168.1.9
> /an 2 5 192.168.1.10
{.is-success}

# Remove a node
It is very simple to remove a node from your account.

```/rn tier number```


# Add Address
Assign the EGEM address so system can watch balance and send transactions to it.

```/setaddress egemaddress```

# Set Paylimit
Pick from 1-100 for when the system will send the transaction containing the EGEM earned when it reaches the limit.

```/paylimit 1-100```

# Toggle Autopay
Set if you want the system to send automatic transactions for earned EGEM.

```/autopay```