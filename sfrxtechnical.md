---
title: Technical Information
description: 
published: true
date: 2020-01-31T13:59:02.150Z
tags: 
---

![egem-sapphire-h-logo-black.png](/egem-sapphire-h-logo-black.png)
# Sapphire / SFRX

> EtherGem’s Sapphire is an integrated subchain with a Decentralized Exchange (DEX). Sapphire is a PoW blockchain with a target 5 seconds block time that observes the EGEM chain while conducting independent business on its own structure. 
{.is-info}

Name: EGEM Sapphire
Ticker: SFRX
Algorithm: PoW Blake2s (custom miner)
Net/Chain Id: 663
BIP-0044
https://github.com/satoshilabs/slips/blob/master/slip-0044.md 
RPC: 1989
P2P: dynamic 
Blocktime: 4 to 5 sec target (depends on Diff ALGO)
Custom Difficulty Algo.

https://bitcointalk.org/index.php?topic=5063954.0