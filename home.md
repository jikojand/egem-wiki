---
title: Home
description: 
published: true
date: 2019-12-01T22:24:31.135Z
tags: 
---

![egem-logo.png](/egem-logo.png)
# Welcome

> Ethergem wiki and database for information.
{.is-info}

 
Thank you for visiting! please select a option from the left menu.
We are currently migrating all info into this new Wiki. Your patience is appreciated.
Until this migration/creation of the data is completed questions can be asked directly to our team on discord.egem.io
{.is-success}

# Notice


> Please note that the wiki is under active construction as we collect and refine all our info. 
***This wiki is not mobile friendly at this time***
{.is-warning}
