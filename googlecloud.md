---
title: Advanced Guide for Google Cloud
description: Setup multiple VMs quickly using Google Cloud
published: true
date: 2019-12-01T21:58:51.078Z
tags: 
---

> # How to use google cloud and setup an EGEM Quarry Node on multiple vps{.is-info}

## Helpful links
https://cloud.google.com/compute/docs/images
https://cloud.google.com/compute/docs/imagesstyle="text-align:justify;"
https://cloud.google.com/compute/docs/instance-templates
https://cloud.google.com/compute/docs/instance-templates
https://cloud.google.com/compute/docs/instance-templates/create-instance-templates
https://cloud.google.com/compute/docs/instance-templates/create-instance-templates
https://cloud.google.com/compute/docs/instance-groups/
https://cloud.google.com/compute/docs/instance-groups/

First thing I did was to create one base node.

> *By using these instructions, I gather you (the reader) already know how to do this*
{.is-warning}

Install first node.
Stop the node.
Create image.
Make instance template.

> *Make sure to change the boot disk to the image you created*
{.is-warning}

> Before you hit create make sure the pricing is to your liking. Some instances can be pricey and will eat up your credits
{.is-info}

Make an instance group - This will allow the fast deploy of many servers using the previous setup.
> It is recommended to name the group after the region as there is a limit of 3 instances per zone. Make sure to turn off auto scaling.
{.is-info}

Hope this guide has helped.
Now you can use this to scale into other regions.