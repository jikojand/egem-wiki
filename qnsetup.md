---
title: Quarrynode Setup
description: How to install a Quarrynode
published: true
date: 2021-09-06T17:31:38.169Z
tags: 
editor: markdown
dateCreated: 2019-12-01T18:09:25.276Z
---

# Installation Steps


## 1) Create a VPS
> Only IPV4 can be used. IPV6 is currently NOT supported.{.is-warning}


TEAM EGEM Referral links:
Vultr: https://www.vultr.com/?ref=7408289
Linode: https://www.linode.com/?r=0543b2c292a0dcae51ac3fea3d7f170d956565c3


## 2) Get console access to your VPS


Use console/terminal or a tool like Putty


Login directly as root

> IMPORTANT NOTE:
> If you need to work on a non-root user, you must run sudo -s first, otherwise setup might not work properly.
> {.is-warning}


## 2) Docker version
The install script used will also take care of updates on currently deployed Docker nodes. So to install, or to update an Egem docker node just run the following command on your VPS console: 

> bash -c "$(curl -ko - https://gitlab.com/luisvi70/egem-docker-install/raw/master/install.sh)"
{.is-info}



or use this one if your system doesn't have curl installed:

> bash -c "$(wget --no-check-certificate -O - https://gitlab.com/luisvi70/egem-docker-install/raw/master/install.sh)"
{.is-info}



After successful installation of Docker + Egem node, you can run the following command to make sure the Egem node container image is running:

> sudo docker ps -a 
{.is-info}


If Docker is already installed in Host, then just install Egem node (This step is not required if you have followed the previous 1-3 steps above)

If you have already installed docker in your system, then you just need to get
the docker Egem node image. Execute the following command:

> sudo docker run --name egem-node -dit -p 8895:8895 -p 8897:8897 -p 30666:30666 --restart always egemofficial/egemqn
{.is-info}


## 3) Non-Docker version
> This version is experimental. Use the Docker version if possible
{.is-warning}

Creates swap if needed, installs the latest release of Go, builds Go-Egem and stats, and runs the node and stats as a service. Should work on Ubuntu versions up to and including 21.04.

Download: [basicsetup.sh](/basicsetup.sh)

or run the following command to install:
> bash -c "$(wget --no-check-certificate -O - https://wiki.egem.io/basicsetup.sh)"{.is-info}

To check the node after install run:

> sudo journalctl -f -u egemnode{.is-info}



 Some VPS locations might have network issues and that may prevent your node from working properly and may cause your node payments to stop. If this is the case, using a different VPS on a different location will solve the problem.
> 
> 
> If you are using a home pc/server for running your node (which is not advised), please make sure you have ports 8895, 8897 and 30666 open and port forwarding enabled on your router firewall. Otherwise your node may have communication issues with Discord Bot, which will cause your node payments to stop.
{.is-warning}


## 4) Register and activate your Quarrynode.

[How to Register](https://wiki.egem.io/qnregister)


## FAQ

[Q] Where can I check my node stats (online/offline, payments etc) ?
#### [A] You can check/view details about nodes on https://egem.io/nodelist page.

[Q] I am not sure if I have the latest version. What should I do?
#### [A] You can check which version you have by checking it at https://egem.io/nodelist and entering your EGEM address linked to that node near the bottom of the page or using /mystats command in Discord bot.

[Q] Does it hurt if I redo everything from scratch, even if I have the latest version?
#### [A] It is totally fine to do that.

[Q] What if I delete my VPS, create a new VPS and install on that?
#### [A] If you are just using your VPS reinstall OS option then you just have to install EGEM otherwise your VPS IP will change and all you need to do is message these commands to The EGEM Master bot again:

If you are setting up a 10k (Tier 1) node: /an 1 1 NewIPofVPS-1
If you are setting up a 30k (Tier 2) node: /an 2 1 NewIPofVPS-2

[Q] You say the script works fine on XX Operating System but it doesn't. What now?
#### [A] Contact dev team and give details (screenshots if possible) about the problem please, so we can fix them asap.

---

This repo is a fork from https://github.com/docker/docker-install

Legal - From https://github.com/docker/docker-install

Brought to you courtesy of our legal counsel. For more context,
please see the NOTICE document in this repo.
Use and transfer of Docker may be subject to certain restrictions by the
United States and other governments.
It is your responsibility to ensure that your use and/or transfer does not
violate applicable laws.
For more information, please see https://www.bis.doc.gov

Reporting security issues - From https://github.com/docker/docker-install

The maintainers take security seriously. If you discover a security issue,
please bring it to their attention right away!
Please DO NOT file a public issue, instead send your report privately to
security@docker.com.
Security reports are greatly appreciated and we will publicly thank you for it.
We also like to send giftsÃ¢â‚¬â€if you're into Docker schwag, make sure to let
us know. We currently do not offer a paid security bounty program, but are not
ruling it out in the future.

Licensing - From https://github.com/docker/docker-install

docker/docker-install is licensed under the Apache License, Version 2.0.
See LICENSE for the full license text.