---
title: Pecunia Setup
description: Create a server on Pecunia
published: true
date: 2021-09-07T17:24:10.262Z
tags: 
editor: markdown
dateCreated: 2020-05-12T15:48:56.039Z
---

# How to set up a node using Pecunia.

![pecunia_icon.png](/pecunia_icon.png)
[Pecunia](https://pecuniaplatform.io/ref/605c6830)

Nodes are $1.49/month USD or $0.99/month with the 6 month plan
Only crypto is accepted for payment.


---

> There is no authentication to send your EGEM to for Pecunia. If you get a discord message stating so report that person!
{.is-warning}

# Step 1
Create an account.

# Step 2
Add funds.
> Note: Pecunia only accepts crypto currency as payment.
{.is-info}

# Step 3
Search for egem.
![pecuniastep1.png](/pecuniastep1.png)

# Step 4

> You can use T1 nodes for T2 nodes. They are the same. Select T1 for all your nodes!
{.is-warning}

Click right arrow button.
![pecuniastep_2.png](/pecuniaselect.png)

# Step 5
> Your EGEM address is optional, there is no need to enter it.
{.is-info}

In the window that pops up, select your payment plan, click Proceed then Deploy server.


# Step 6
Wait. When the installation is done the information will show up.

# Step 7
Register shown IP into The EGEM Master bot.
[How to Register](https://wiki.egem.io/qnregister)
