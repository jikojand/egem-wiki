---
title: Technical Information
description: 
published: true
date: 2021-09-25T16:09:04.679Z
tags: 
editor: markdown
dateCreated: 2019-12-01T21:01:12.637Z
---

# About Quarrynodes

- Custom masternode system using [discord.js](https://discord.js.org/#/) / [web3.js](https://web3js.readthedocs.io/en/1.0/) / [telegraf.js](https://telegraf.js.org/) with a MySQL database for storage.

https://gitlab.com/ethergem/egem-bot

## Features
Just some of the minor things to mention.
- Privacy due to the private message nature of the system.
- Reliable network of updatable nodes to maintain stability and security.
- Almost all parts of the system have been designed by members of the community and their input.


# Collateral Requirements
## Tier One (Quarrynode)
> 10,000 EGEM Collateral
{.is-success}

## Tier Two (Geode)
> Tier One Node & 30,000 EGEM totalling 40,000 EGEM Collateral
(You must run both nodes)
{.is-success}




# Hardware Requirements

Minimum Requirements:

> CPU: 2 core
> RAM: 4 GB total between RAM + swap
> Disk: 20 GB HDD/SDD
> Bandwidth: 250 GB per month
> Operating System: Ubuntu 18/16, CentOS 7, Debian 9 or Raspbian 9
{.is-success}

