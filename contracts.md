---
title: Smart Contracts and Tokens
description: 
published: true
date: 2019-12-04T15:22:11.614Z
tags: 
---

# Smart Contract and Tokens

# Requirements
## EGEM Address with Balance
[EGEM to Deploy](https://wiki.egem.io/Links#exchanges)
## Text Editor
[Sublime Text](https://www.sublimetext.com/)
[Atom](https://atom.io/)
## Wallet
[Metamask](https://wiki.egem.io/egemwallet#metamask)
## Compiler
[Remix](http://remix.ethereum.org)

# Write Contract and Deploy
## Example Tokens and Smart Contracts
https://www.proofsuite.com/smartcontract/
https://github.com/bitfwdcommunity/ICO-tutorial/blob/master/ico-contract.sol

## Compile and Create Contract
[Remix](http://remix.ethereum.org)

# Tutorial
## Token Creation
todo
## Staking Token Creation
todo
## Mineable Token Creation
todo

# Examples
## sample-token.sol
```contract Token {
    /* Public variables of the token */
    string public standard = 'Token 0.1';
    string public name;
    string public symbol;
    uint8 public decimals;
    uint256 public initialSupply;
    uint256 public totalSupply;

    /* This creates an array with all balances */
    mapping (address => uint256) public balanceOf;
    mapping (address => mapping (address => uint256)) public allowance;

  
    /* Initializes contract with initial supply tokens to the creator of the contract */
    function Token() {

         initialSupply = 500;
         name ="proofcoin";
        decimals = 2;
         symbol = "^";
        
        balanceOf[msg.sender] = initialSupply;              // Give the creator all initial tokens
        totalSupply = initialSupply;                        // Update total supply
                                   
    }

    /* Send coins */
    function transfer(address _to, uint256 _value) {
        if (balanceOf[msg.sender] < _value) throw;           // Check if the sender has enough
        if (balanceOf[_to] + _value < balanceOf[_to]) throw; // Check for overflows
        balanceOf[msg.sender] -= _value;                     // Subtract from the sender
        balanceOf[_to] += _value;                            // Add the same to the recipient
      
    }

   

    

   

    /* This unnamed function is called whenever someone tries to send ether to it */
    function () {
        throw;     // Prevents accidental sending of ether
    }
}```